package br.com.filehandler.scanner;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.HashMap;
import java.util.Map;

import br.com.filehandler.engine.FileCore;
import br.com.filehandler.util.WarningType;

public class FileScanner {

	@SuppressWarnings(WarningType.RAWTYPES)
	public void scan(Path inputDirectory, Path outputDirectory, WatchEvent.Kind[] watchEvents, String... extensions) {

		try {
			final WatchService watcher = FileSystems.getDefault().newWatchService();
			final Map<WatchKey, Path> keys = this.register(inputDirectory, watcher, watchEvents);
			this.processEvents(watcher, keys, outputDirectory, extensions);
		} catch (IOException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}

	@SuppressWarnings(WarningType.RAWTYPES)
	private Map<WatchKey, Path> register(Path inDir, WatchService watcher, WatchEvent.Kind[] watchEvents) throws IOException {
		final Map<WatchKey, Path> keys = new HashMap<>();
		final WatchKey watchKey = inDir.register(watcher, watchEvents);
		keys.put(watchKey, inDir);
		return keys;
	}

	public void processEvents(WatchService watcher, Map<WatchKey, Path> keys, Path outputDirectory, String... extensions) {

		while (true) {

			final WatchKey key;
			try {
				key = watcher.take();
			} catch (InterruptedException x) {
				return;
			}

			final Path dir = keys.get(key);
			if (dir == null) {
				System.err.println("WatchKey not recognized!!");
				continue;
			}

			FileCore.handleFile(dir, keys, key, outputDirectory, extensions);

			boolean valid = key.reset();
			if (!valid) {
				keys.remove(key);

				if (keys.isEmpty()) {
					break;
				}
			}
		}
	}


}
