package br.com.filehandler.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class Employee extends PhysicalPerson implements Serializable {

	private static final long serialVersionUID = 7278560532669354760L;

	private BigDecimal wage;

	public Employee(String taxId, String name, BigDecimal wage) {
		super(taxId, name);
		this.wage = wage;
	}

	public BigDecimal getWage() {
		return wage;
	}

	public void setWage(BigDecimal wage) {
		this.wage = wage;
	}

	@Override
	public String toString() {
		return "Employee [wage=" + wage + ", taxId=" + getTaxId() + ", name=" + getName() + "]";
	}

}
