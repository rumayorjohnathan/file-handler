package br.com.filehandler.model;

import java.io.Serializable;

public abstract class PhysicalPerson implements Serializable {

	private static final long serialVersionUID = -487120319774913095L;

	private String taxId;
	private String name;

	private PhysicalPerson() {
		super();
	}

	protected PhysicalPerson(String taxId, String name) {
		this();
		this.taxId = taxId;
		this.name = name;
	}

	public String getTaxId() {
		return taxId;
	}

	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "PhysicalPerson [taxId=" + taxId + ", name=" + name + "]";
	}

}
