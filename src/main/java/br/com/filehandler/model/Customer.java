package br.com.filehandler.model;

public class Customer extends PhysicalPerson {

	private static final long serialVersionUID = -3648497460782852687L;

	private String businessArea;

	public Customer(String taxId, String name, String businessArea) {
		super(taxId, name);
		this.businessArea = businessArea;
	}

	public String getBusinessArea() {
		return businessArea;
	}

	public void setBusinessArea(String businessArea) {
		this.businessArea = businessArea;
	}

	@Override
	public String toString() {
		return "Customer [businessArea=" + businessArea + ", getTaxId()=" + getTaxId() + ", getName()=" + getName()
				+ ", toString()=" + super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ "]";
	}

}
