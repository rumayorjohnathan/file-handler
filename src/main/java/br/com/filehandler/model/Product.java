package br.com.filehandler.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class Product implements Serializable {

	private static final long serialVersionUID = -6697747793631565582L;

	private Integer id;
	private BigDecimal value;

	public Product() {
		super();
	}

	public Product(Integer id, BigDecimal value) {
		this();
		this.id = id;
		this.value = value;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", value=" + value + "]";
	}

}
