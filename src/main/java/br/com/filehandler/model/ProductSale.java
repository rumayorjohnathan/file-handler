package br.com.filehandler.model;

import java.io.Serializable;

public class ProductSale implements Serializable {

	private static final long serialVersionUID = -2559178922312060711L;

	private Integer productAmount;
	private Product product;

	public ProductSale() {
		super();
	}

	public ProductSale(Integer productAmount, Product product) {
		this();
		this.productAmount = productAmount;
		this.product = product;
	}

	public Integer getProductAmount() {
		return productAmount;
	}

	public void setProductAmount(Integer productAmount) {
		this.productAmount = productAmount;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Override
	public String toString() {
		return "ProductSale [productAmount=" + productAmount + ", product=" + product + "]";
	}

}
