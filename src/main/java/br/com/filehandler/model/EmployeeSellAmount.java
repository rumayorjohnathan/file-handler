package br.com.filehandler.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class EmployeeSellAmount implements Serializable {

	private static final long serialVersionUID = -4057361905218401273L;

	private Employee employee;
	private BigDecimal saleValueAmount;

	public EmployeeSellAmount() {
		super();
	}

	public EmployeeSellAmount(Employee employee, BigDecimal saleValueAmount) {
		this();
		this.employee = employee;
		this.saleValueAmount = saleValueAmount;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public BigDecimal getSaleValueAmount() {
		return saleValueAmount;
	}

	public void setSaleValueAmount(BigDecimal saleValueAmount) {
		this.saleValueAmount = saleValueAmount;
	}

	@Override
	public String toString() {
		return "EmployeeSellAmount [employee=" + employee + ", saleValueAmount=" + saleValueAmount + "]";
	}

}
