package br.com.filehandler.model;

import java.io.Serializable;

public class FileSummary implements Serializable {

	private static final long serialVersionUID = -6815841162212420602L;

	private Integer customerAmount;
	private Integer employeeAmount;
	private Sale mostExpensiveSale;
	private Employee worstEmployee;

	public FileSummary() {
		super();
	}

	public FileSummary(Integer customerAmount, Integer employeeAmount, Sale mostExpensiveSale,
			Employee worstEmployee) {
		this();
		this.customerAmount = customerAmount;
		this.employeeAmount = employeeAmount;
		this.mostExpensiveSale = mostExpensiveSale;
		this.worstEmployee = worstEmployee;
	}

	public Integer getCustomerAmount() {
		return customerAmount;
	}

	public void setCustomerAmount(Integer customerAmount) {
		this.customerAmount = customerAmount;
	}

	public Integer getEmployeeAmount() {
		return employeeAmount;
	}

	public void setEmployeeAmount(Integer employeeAmount) {
		this.employeeAmount = employeeAmount;
	}

	public Sale getMostExpensiveSale() {
		return mostExpensiveSale;
	}

	public void setMostExpensiveSale(Sale mostExpensiveSale) {
		this.mostExpensiveSale = mostExpensiveSale;
	}

	public Employee getWorstEmployee() {
		return worstEmployee;
	}

	public void setWorstEmployee(Employee worstEmployee) {
		this.worstEmployee = worstEmployee;
	}

	@Override
	public String toString() {
		return "FileSummary [customerAmount=" + customerAmount + ", employeeAmount=" + employeeAmount
				+ ", mostExpensiveSale=" + mostExpensiveSale + ", worstEmployee=" + worstEmployee + "]";
	}

}
