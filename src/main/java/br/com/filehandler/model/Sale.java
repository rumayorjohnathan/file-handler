package br.com.filehandler.model;

import java.beans.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class Sale implements Serializable {

	private static final long serialVersionUID = -8308952492630020482L;

	private Integer id;
	private List<ProductSale> productSale;
	private Employee employee;

	public Sale() {
		super();
	}

	public Sale(Integer id, List<ProductSale> productSale, Employee employee) {
		this();
		this.id = id;
		this.productSale = productSale;
		this.employee = employee;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public List<ProductSale> getProductSale() {
		return productSale;
	}

	public void setProductSale(List<ProductSale> productSale) {
		this.productSale = productSale;
	}

	@Transient
	public BigDecimal getSaleValue() {
		BigDecimal saleValue = BigDecimal.ZERO;
		for (ProductSale ps: productSale) {
			saleValue = saleValue.add(ps.getProduct().getValue().multiply(BigDecimal.valueOf(ps.getProductAmount())));
		}
		return saleValue;
	}

	@Override
	public String toString() {
		return "Sale [id=" + id + ", productSelling=" + productSale + ", employee=" + employee + "]";
	}

}
