package br.com.filehandler.engine;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import br.com.filehandler.model.Customer;
import br.com.filehandler.model.Employee;
import br.com.filehandler.model.EmployeeSellAmount;
import br.com.filehandler.model.Product;
import br.com.filehandler.model.Sale;

public class FileGrouping {

	private final String fileName;
	private final Map<String, Employee> employees;
	private final Map<Integer, Product> products;
	private final Map<Integer, Sale> sales;
	private final Map<String, Customer> customers;
	private Sale mostExpensiveSale;
	private Employee worstEmployee;

	public FileGrouping(String fileName) {
		super();
		this.fileName = fileName;
		this.employees = new HashMap<String, Employee>();
		this.products = new HashMap<Integer, Product>();
		this.sales = new HashMap<Integer, Sale>();
		this.customers = new HashMap<String, Customer>();
		this.mostExpensiveSale = null;
		this.worstEmployee = null;
	}

	public void addEmployee(Employee employee) {
		final String employeeName = employee.getName();
		if (!this.employees.containsKey(employeeName)) {
			employees.put(employeeName, employee);
		}
	}

	public void addProduct(Product product) {
		final Integer productId = product.getId();
		if (!this.products.containsKey(productId)) {
			products.put(productId, product);
		}
	}

	public void addSale(Sale sale) {
		final Integer saleId = sale.getId();
		if (!this.sales.containsKey(saleId)) {
			sales.put(saleId, sale);
		}
		updateMostExpensiveSale(sale);
		updateWorstEmployee(sale);
	}

	public void addCustomer(Customer customer) {
		final String taxId = customer.getTaxId();
		if (!this.customers.containsKey(taxId)) {
			customers.put(taxId, customer);
		}
	}

	public String getFileName() {
		return fileName;
	}

	public Map<String, Employee> getEmployees() {
		return employees;
	}

	public Map<Integer, Product> getProducts() {
		return products;
	}

	public Map<Integer, Sale> getSales() {
		return sales;
	}

	public Map<String, Customer> getCustomers() {
		return customers;
	}

	public Sale getMostExpensiveSale() {
		return mostExpensiveSale;
	}

	public Employee getWorstEmployee() {
		return worstEmployee;
	}

	private void updateMostExpensiveSale(Sale sale) {
		if (this.mostExpensiveSale == null) {
			this.mostExpensiveSale = sale;
		}
		this.mostExpensiveSale = this.sales.values().stream().max(Comparator.comparing(Sale::getSaleValue)).get();
	}

	private void updateWorstEmployee(Sale sale) {
		if (this.worstEmployee == null) {
			this.worstEmployee = employees.get(sale.getEmployee().getName());
		}
		this.worstEmployee = employees.values()
		.stream()
		.map(e -> new EmployeeSellAmount(e, getSalesValueByEmployee(e)))
		.min(Comparator.comparing(EmployeeSellAmount::getSaleValueAmount))
		.get()
		.getEmployee();
	}

	private BigDecimal getSalesValueByEmployee(Employee employee) {
		return this.sales
		.values()
		.stream()
		.filter(sale -> employee.getTaxId().equals(sale.getEmployee().getTaxId()))
		.collect(Collectors.toList())
		.stream()
		.map(Sale::getSaleValue)
		.reduce(BigDecimal.ZERO, BigDecimal::add);
	}

	@Override
	public String toString() {
		return "FileGrouping [fileName=" + fileName + ", employees=" + employees + ", products=" + products + ", sales="
				+ sales + ", customers=" + customers + ", mostExpensiveSale=" + mostExpensiveSale + ", worstEmployee="
				+ worstEmployee + "]";
	}

}
