package br.com.filehandler.engine;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import br.com.filehandler.model.Customer;
import br.com.filehandler.model.Employee;
import br.com.filehandler.model.Product;
import br.com.filehandler.model.ProductSale;
import br.com.filehandler.model.Sale;

public class FileReader {

	private static final int EMPLOYEE_KEY = 001;
	private static final int CUSTOMER_KEY = 002;
	private static final int SALE_KEY = 003;

	private final String separator;

	public FileReader(String separator) {
		super();
		this.separator = separator;
	}

	public FileGrouping getFileGrouping(Path filePath) {
		final BufferedReader reader = this.init(filePath);
		final FileGrouping fileGrouping = this.processFileGrouping(reader, filePath);
		this.finish(reader);
		return fileGrouping;
	}

	private BufferedReader init(Path filePath) {
		BufferedReader reader = null;
		try {
			reader = Files.newBufferedReader(filePath);
		} catch(IOException e) {
			e.printStackTrace();
		}
		return reader;
	}
	
	private void finish(BufferedReader reader) {
		try {
			reader.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	private FileGrouping processFileGrouping(BufferedReader reader, Path filePath) {
		final FileGrouping fileGrouping = new FileGrouping(filePath.getFileName().toString());

		try {
			while(reader.ready()) {
				final StringTokenizer tokenizer = new StringTokenizer(reader.readLine(), this.separator);
				final Integer format = Integer.parseInt(tokenizer.nextToken());
				
				switch(format) {
					case EMPLOYEE_KEY: {
						fileGrouping.addEmployee(buildEmployee(tokenizer));
						break;
					}
					case CUSTOMER_KEY: {
						fileGrouping.addCustomer(buildCustomer(tokenizer));
						break;
					}
					case SALE_KEY: {
						fileGrouping.addSale(buildSale(tokenizer, fileGrouping));
						break;
					}
				}
			}
		} catch(IOException e) {
			e.printStackTrace();
		}
		System.out.println(String.format("Generated data from file: %s, %s", filePath.getFileName().toString(), fileGrouping.toString()));
		return fileGrouping;
	}

	private Employee buildEmployee(StringTokenizer tokenizer) {
		final String taxId = tokenizer.nextToken();
		final String name = tokenizer.nextToken();
		final BigDecimal wage = BigDecimal.valueOf(Double.valueOf(tokenizer.nextToken()));
		return new Employee(taxId, name, wage);
	}

	private Customer buildCustomer(StringTokenizer tokenizer) {
		final String taxId = tokenizer.nextToken();
		final String name = tokenizer.nextToken();
		final String businessArea = tokenizer.nextToken();

		return new Customer(taxId, name, businessArea);
	}

	private Sale buildSale(StringTokenizer tokenizer, FileGrouping fileGrouping) {
		final Integer id = Integer.parseInt(tokenizer.nextToken());
		final List<ProductSale> saleItems = this.processSalesItems(tokenizer.nextToken());
		final Employee employee = fileGrouping.getEmployees().get(tokenizer.nextToken());

		return new Sale(id, saleItems, employee);
	}

	private List<ProductSale> processSalesItems(String saleItemsStr) {
		saleItemsStr = saleItemsStr.substring(1, saleItemsStr.length() - 1);
		final List<ProductSale> saleItems = new ArrayList<>();

		final StringTokenizer tokenizer = new StringTokenizer(saleItemsStr, ",");
		while(tokenizer.hasMoreTokens()) {
			saleItems.add(this.processSaleItem(tokenizer.nextToken()));
		}
		return saleItems;
	}

	private ProductSale processSaleItem(String saleItemStr) {
		final StringTokenizer tokenizer = new StringTokenizer(saleItemStr, "-");
		final Integer id = Integer.parseInt(tokenizer.nextToken()); 
		final Integer amount = Integer.parseInt(tokenizer.nextToken()); 
		final BigDecimal value = BigDecimal.valueOf(Double.valueOf(tokenizer.nextToken())); 

		return new ProductSale(amount, new Product(id, value));
	}
}
