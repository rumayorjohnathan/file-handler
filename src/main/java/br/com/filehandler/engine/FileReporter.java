package br.com.filehandler.engine;

import br.com.filehandler.model.Employee;
import br.com.filehandler.model.FileSummary;
import br.com.filehandler.model.Sale;

public class FileReporter {

	public static FileSummary getFileSummary(FileGrouping fileGrouping) {

		final Integer customersAmount = fileGrouping.getCustomers().keySet().size();
		final Integer employeeAmount = fileGrouping.getEmployees().keySet().size();
		final Sale mostExpensiveSale = fileGrouping.getMostExpensiveSale();
		final Employee worstEmployee = fileGrouping.getWorstEmployee();

		return new FileSummary(customersAmount, employeeAmount, mostExpensiveSale, worstEmployee);
	}
}
