package br.com.filehandler.engine;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;

import br.com.filehandler.model.FileSummary;

public class FileWriter {

	public static void writeFileReport(String fileName, FileSummary fileSummary, File outputDirectory) {
		fileName = fileName.substring(0, fileName.length() - 4);
		fileName = fileName + ".ok.dat";

		if(!outputDirectory.exists()) {
			outputDirectory.mkdirs();
		}

		final File outFile = new File(outputDirectory, fileName);
		try(BufferedWriter writer = new BufferedWriter(new java.io.FileWriter(outFile))) {
			writer.write(String.format("Customers Amount: %d\n", fileSummary.getCustomerAmount()));
			writer.write(String.format("Employee Amount: %d\n", fileSummary.getEmployeeAmount()));
			writer.write(String.format("Most expensive sale: %d\n", fileSummary.getMostExpensiveSale().getId()));
			writer.write(String.format("Worst employee ever: %s\n", fileSummary.getWorstEmployee()));
			writer.flush();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

}
