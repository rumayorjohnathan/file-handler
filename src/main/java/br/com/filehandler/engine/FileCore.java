package br.com.filehandler.engine;

import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import br.com.filehandler.model.FileSummary;
import br.com.filehandler.util.WarningType;

public class FileCore {

	@SuppressWarnings(WarningType.RAWTYPES)
	public static void handleFile(Path dir, Map<WatchKey, Path> keys, WatchKey key, Path outputDirectory, String... extensions ) {
		final List<String> fileExtensionsList = Arrays.asList(extensions);
		for (WatchEvent<?> pollEvent : key.pollEvents()) {
			final WatchEvent.Kind kind = pollEvent.kind();

			if (kind == StandardWatchEventKinds.OVERFLOW) {
				continue;
			}

			final WatchEvent<Path> event = cast(pollEvent);
			final Path name = event.context();
			final Path filePath = dir.resolve(name);

			System.out.println(String.format("Parsing file with name: %s", filePath.getFileName()));

			if (filePath.toFile().exists()) {
				final String fileName = filePath.toString();
				final String fileExtension = fileName.toLowerCase().substring(fileName.length() - 3, fileName.length());
				if (fileExtensionsList.contains(fileExtension)) {
					generateFile(fileName, filePath, outputDirectory);
				}
			}
		}

	}

	@SuppressWarnings(WarningType.UNCHECKED)
	private static <T> WatchEvent<T> cast(WatchEvent<?> event) {
		return (WatchEvent<T>) event;
	}

	public static void generateFile(String fileName, Path filePath, Path outputDirectory) {
		final FileReader fileReader = new FileReader("ç");
		final FileGrouping fileGrouping = fileReader.getFileGrouping(filePath);
		final FileSummary fileSummary = FileReporter.getFileSummary(fileGrouping);
		FileWriter.writeFileReport(fileGrouping.getFileName(), fileSummary, outputDirectory.toFile());
	}
}
