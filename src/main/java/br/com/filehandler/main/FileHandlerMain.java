package br.com.filehandler.main;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;

import br.com.filehandler.scanner.FileScanner;

public class FileHandlerMain {

	private static final String HOME = "HOMEPATH";
	private static final String RES = "data";

	public static void main(String[] args) {
		final String homepath = System.getenv(HOME);
		final Path inDir = Paths.get(homepath + File.separator + RES + File.separator + "in");
		final Path outDir = Paths.get(homepath + File.separator, RES + File.separator + "out");
		System.out.println(String.format("Starting scan to referenced folder: %s", homepath));

		new FileScanner().scan(inDir, outDir, new WatchEvent.Kind[] { StandardWatchEventKinds.ENTRY_MODIFY }, new String[] { "dat", "txt" });
	}
}
