package br.com.filehandler.engine;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import br.com.filehandler.model.Customer;
import br.com.filehandler.model.Employee;
import br.com.filehandler.model.Product;
import br.com.filehandler.model.ProductSale;
import br.com.filehandler.model.Sale;

public class FileGroupingTest {

	private FileGrouping fileGrouping;
	private Employee employee1;
	private Employee employee2;
	private Customer customer1;
	private Customer customer2;
	private List<ProductSale> sale1Products; 
	private List<ProductSale> sale2Products;
	private Sale sale1;
	private Sale sale2;

	@Before
	public void setUp() {
		fileGrouping = new FileGrouping("TEST.dat");
		this.employee1 = new Employee("12345678910", "Paulo Victor", BigDecimal.TEN);
		this.employee2 = new Employee("12345678911", "Julio Cesar", BigDecimal.ONE);
		this.customer1 = new Customer("12345678910", "Renato Portaluppi", "Cidade");
		this.customer2 = new Customer("99999999999", "Romildo Bolzan", "Rural");
		this.sale1Products = Collections.singletonList(new ProductSale(10, new Product(15, BigDecimal.TEN)));
		this.sale2Products = Collections.singletonList(new ProductSale(11, new Product(20, BigDecimal.ONE)));
		this.sale1 = new Sale(1, this.sale1Products, employee1);
		this.sale2 = new Sale(2, this.sale2Products, employee2);
	}

	@Test
	public void addEmployeeOkTest() {
		fileGrouping.addEmployee(employee1);
		assertEquals(1, this.fileGrouping.getEmployees().keySet().size());
		fileGrouping.addEmployee(employee2);
		assertEquals(2, this.fileGrouping.getEmployees().keySet().size());
	}

	@Test
	public void addEmployeeRepeatedTest() {
		fileGrouping.addEmployee(this.employee1);
		fileGrouping.addEmployee(this.employee1);
		assertEquals(1, this.fileGrouping.getEmployees().keySet().size());
	}

	@Test
	public void addCustomerOkTest() {
		fileGrouping.addCustomer(this.customer1);
		assertEquals(1, this.fileGrouping.getCustomers().keySet().size());
		fileGrouping.addCustomer(this.customer2);
		assertEquals(2, this.fileGrouping.getCustomers().keySet().size());
	}

	@Test
	public void addCustomerRepeatedTest() {
		fileGrouping.addCustomer(this.customer1);
		assertEquals(1, this.fileGrouping.getCustomers().keySet().size());
		fileGrouping.addCustomer(this.customer1);
		assertEquals(1, this.fileGrouping.getCustomers().keySet().size());
	}

	@Test
	public void addSaleOkTest() {
		fileGrouping.addEmployee(this.employee1);
		fileGrouping.addEmployee(this.employee2);

		fileGrouping.addSale(this.sale1);
		assertEquals(1, fileGrouping.getSales().size());
		fileGrouping.addSale(sale2);
		assertEquals(2, fileGrouping.getSales().size());
	}

	@Test
	public void addSaleRepeatedTest() {
		fileGrouping.addEmployee(this.employee1);

		fileGrouping.addSale(this.sale1);
		assertEquals(1, fileGrouping.getSales().size());
		fileGrouping.addSale(this.sale1);
		assertEquals(1, fileGrouping.getSales().size());
	}

	@Test
	public void checkBusinessRulesOkTest() {
		fileGrouping.addEmployee(this.employee1);
		fileGrouping.addEmployee(this.employee2);

		fileGrouping.addSale(this.sale1);
		assertEquals(1, fileGrouping.getSales().size());
		fileGrouping.addSale(this.sale2);
		assertEquals(2, fileGrouping.getSales().size());

		assertEquals(employee2, fileGrouping.getWorstEmployee());
		assertEquals(sale1, fileGrouping.getMostExpensiveSale());
	}

}
