package br.com.filehandler.engine;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import br.com.filehandler.model.Customer;
import br.com.filehandler.model.Employee;
import br.com.filehandler.model.FileSummary;
import br.com.filehandler.model.Product;
import br.com.filehandler.model.ProductSale;
import br.com.filehandler.model.Sale;

public class FileReportTest {

	private FileGrouping fileGrouping;
	private Employee employee1;
	private Employee employee2;
	private Customer customer1;
	private Customer customer2;
	private List<ProductSale> sale1Products; 
	private List<ProductSale> sale2Products;
	private Sale sale1;
	private Sale sale2;

	@Before
	public void setUp() {
		fileGrouping = new FileGrouping("TEST.dat");

		this.employee1 = new Employee("12345678910", "Paulo Victor", BigDecimal.TEN);
		this.employee2 = new Employee("12345678911", "Julio Cesar", BigDecimal.ONE);
		this.fileGrouping.addEmployee(employee1);
		this.fileGrouping.addEmployee(employee2);

		this.customer1 = new Customer("12345678910", "Renato Portaluppi", "Cidade");
		this.customer2 = new Customer("99999999999", "Romildo Bolzan", "Rural");
		this.fileGrouping.addCustomer(customer1);
		this.fileGrouping.addCustomer(customer2);

		this.sale1Products = Collections.singletonList(new ProductSale(10, new Product(15, BigDecimal.TEN)));
		this.sale2Products = Collections.singletonList(new ProductSale(11, new Product(20, BigDecimal.ONE)));
		this.sale1 = new Sale(1, this.sale1Products, employee1);
		this.sale2 = new Sale(2, this.sale2Products, employee2);
		this.fileGrouping.addSale(sale1);
		this.fileGrouping.addSale(sale2);
	}

	@Test
	public void getFileSummaryOkTest() {
		final FileSummary fileSummary = FileReporter.getFileSummary(this.fileGrouping);
		final Integer expected = 2;
		assertEquals(expected, fileSummary.getCustomerAmount());
		assertEquals(expected, Integer.valueOf(fileSummary.getEmployeeAmount()));
		assertEquals(this.sale1, fileSummary.getMostExpensiveSale());
		assertEquals(this.employee2, fileSummary.getWorstEmployee());;
	}
}
