package br.com.filehandler.engine;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

public class FileReaderTest {

	private FileReader fileReader;
	private FileGrouping fileGrouping;

	@Before
	public void setUp() {
		this.fileReader = new FileReader("ç");
	}

	@Test
	public void test() {
		this.fileGrouping = fileReader.getFileGrouping(Paths.get("data" + File.separator + "in" + File.separator + "file.dat"));
		final Integer mostExpensiveSaleExpected = 10;
		assertNotNull(fileGrouping);
		assertEquals(2, fileGrouping.getCustomers().size());
		assertEquals(2, fileGrouping.getEmployees().size());
		assertEquals(mostExpensiveSaleExpected, fileGrouping.getMostExpensiveSale().getId());
		assertEquals("3245678865434", fileGrouping.getWorstEmployee().getTaxId());
	}
}
