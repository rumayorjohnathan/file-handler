package br.com.filehandler.engine;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

import br.com.filehandler.model.FileSummary;

public class FileWriterTest {

	private FileReader fileReader;
	private FileGrouping fileGrouping;

	@Before
	public void setUp() {
		this.fileReader = new FileReader("ç");
	}

	@Test
	public void test() {
		this.fileGrouping = fileReader.getFileGrouping(Paths.get("data" + File.separator + "in" + File.separator + "file.dat"));
		final FileSummary fileSummary = FileReporter.getFileSummary(fileGrouping);
		FileWriter.writeFileReport("file.dat", fileSummary, Paths.get("data" + File.separator + "out" + File.separator).toFile());
		final File file = new File("data" + File.separator + "in" + File.separator + "file.dat");
		assertEquals(Boolean.TRUE, file.exists());
	}
}
